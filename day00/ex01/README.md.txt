Each permission setting can be represented by a numerical value:r=4,w=2,x=1,-=0;
When these values are added together, the total is used to set specific permissions.
The sum is count like this:
 -  (rw-)   (rw-)  (r--)
      |       |      |
    4+2+0   4+2+0  4+0+0
We use chmod (the sum from above) filename to change the permission of the file.